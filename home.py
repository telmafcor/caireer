import streamlit as st
import openai_api_client as cli

st.set_page_config(layout="wide")

with st.container():
    col1, col2, col3 = st.columns([0.35, 0.05, 0.6])

    with col1:
        st.title('cAIreer Copilot')
        st.title('')
        st.image('https://wpminds.com/wp-content/uploads/2022/10/undraw_Maker_launch_re_rq81-1-1024x694-1.png')
        st.title('')

        st.subheader("🚀 Elevate Your Career Journey 🚀")

        with st.container():
            st.text("Your career aspirations are our mission. \n ")
            st.caption("Career Copilot, is here to guide you through every twist and turn, helping you to elevate you "
                       "to new professional heights. Whether you're looking to climb"
                       "the corporate ladder or embark on a bold career change, we've got you covered.")

    with col2:
        st.title('')

    with col3:
        with st.container():
            st.divider()

            with st.container():
                col1, col2, col3 = st.columns([0.6, 0.3, 0.1])

                with col1:
                    st.header('Grow in my current role')
                    st.write('Take steps to succeed in your current role by developing your skills.')

                with col2:
                    st.header('')
                    st.header('')
                    st.link_button('Go to Coach', 'coach_grow')

                with col3:
                    st.write('')
            st.divider()

        with st.container():
            with st.container():
                col1, col2, col3 = st.columns([0.6, 0.3, 0.1])

                with col1:
                    st.header('Transition to a new Job')
                    st.write('Find new opportunities where I am')

                with col2:
                    st.header('')
                    st.header('')
                    st.link_button('Go to Coach', 'coach_transition')

                with col3:
                    st.write('')
        st.divider()

        with st.container():
            with st.container():
                col1, col2, col3 = st.columns([0.6, 0.3, 0.1])

                with col1:
                    st.header('Pursue a new career')
                    st.write('Start a new career in other industries or fields')

                with col2:
                    st.header('')
                    st.header('')
                    st.link_button('Go to Coach', 'http://google.com')

                with col3:
                    st.write('')
        st.divider()

        with st.container():
            with st.container():
                col1, col2, col3 = st.columns([0.6, 0.3, 0.1])

                with col1:
                    st.header('I do not know yet')
                    st.write('I want to explore before I commit to something')

                with col2:
                    st.header('')
                    st.header('')
                    st.link_button('Go to Coach', 'http://youtube.com')

                with col3:
                    st.write('')
        st.divider()
