import streamlit as st
import openai_api_client as cli

st.set_page_config(layout="wide")

response = ''

if 'clicked' not in st.session_state:
    st.session_state.clicked = False

with st.container():
    col1, col2, col3 = st.columns([0.25, 0.01, 0.64])

    with col1:
        st.title('cAIreer Copilot')

        with st.container():
            st.header("Let's kickstart your career transition.")
            st.write("Ready to take off? Fill the form to get your guide")
            st.divider()

            career = st.selectbox("Which job are you seeking?",
                                  ("Software Tester", "Data Scientist", "Product Manager", "Frontend Developer",
                                   "Backend Developer", "Product Designer", "DevOps"),
                                  index=0)

            knowledgeLevel = st.radio(
                'Do you have some knowledge in this field?',
                ['None', "Basic", "Average", "Advanced"])

            skills = st.multiselect("What are your skills?",
                                    ["Networking" "Infrastructure", "CI/CD",
                                     "Programming languages", "Web frameworks",
                                     "Cybersecurity", "Data Management", "Project Management", "UI/UX"]);

            skillsOther = st.text_area('Other skills you would like to mention?')

            system_behavior = 'You are a career coach living in Portugal'
            prompt = f'''Taking into account these career profile of {career} with my knowledge base being 
            {knowledgeLevel} and the following Skills are: {skills} {skillsOther}. Write me a career path recommendation 
            structure between learning recommendations, communities and coaching groups in Portugal, youtube channels, 
            and books to transition to {career} role'''

            def click_button():
                st.session_state.clicked = True


            st.button('Generate', on_click=click_button)

            if st.session_state.clicked:
                response = cli.send_request(system_behavior, prompt)

    with col2:
        st.text('')

    with col3:
        st.divider()
        with st.container():
            st.subheader('cAIreer advisor for transition to a new job')
            st.divider()
            st.write(response)
