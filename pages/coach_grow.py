import streamlit as st
import openai_api_client as cli

st.set_page_config(layout="wide")

response = ''

if 'clicked' not in st.session_state:
    st.session_state.clicked = False

with st.container():
    col1, col2, col3 = st.columns([0.25, 0.01, 0.64])

    with col1:
        st.title('cAIreer Copilot')

        with st.container():
            st.header("Let's kickstart your career growth.")
            st.write("Ready to take off? Fill the form to start your career journey.")
            st.divider()

            role = st.text_input('My current job title')

            yearsOfExperience = st.number_input('My years of experience', 0, 20)

            skills = st.multiselect("What are your skills?",
                                    ["Networking" "Infrastructure", "CI/CD",
                                     "Programming languages", "Web frameworks",
                                     "Cybersecurity", "Data Management", "Project Management", "UI/UX"]);

            skillsOther = st.text_area('Other skills you would like to mention?')

            timeline = st.radio(
                'Timeline Plan',
                ['3 months', "6 months", "9 months"])

            system_behavior = f'''You are a career coach living in Portugal, Give me a table with a career plan 
            for {timeline}'''
            prompt = f'''Taking into account these career profile {role} with {yearsOfExperience} year of experience 
            and the following Skills {skills} {skillsOther}. Make a recommendation career plan should be structured 
            by learning courses, communities and coaching groups in Portugal, youtube channels, and books with url 
            to the sources to grow my career.'''


            def click_button():
                st.session_state.clicked = True


            st.button('Generate', on_click=click_button)

            if st.session_state.clicked:
                response = cli.send_request(system_behavior, prompt)
    with col2:
        st.text('')

    with col3:
        st.divider()
        with st.container():
            st.subheader('cAIreer advisor for grow in my current role')
            st.divider()
            st.write(response)
