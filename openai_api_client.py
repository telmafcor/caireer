
def send_request(system_behavior, prompt):
    import openai
    import os

    openai.api_key = os.getenv('OPENAI_API_KEY')

    # Create the messages (body)
    messages = [
        {'role': 'system', 'content': system_behavior},
        {'role': 'user', 'content': prompt}
    ]

    # Send Request
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=messages,
        temperature=0.8
    )

    return response.choices[0].message.content
