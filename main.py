import streamlit as st
import openai_api_client as cli

st.title("Welcome to cAIreer")

career = st.selectbox(
    "Select the IT Career?",
    ("Software Tester", "Data Scientist"),
    index=0
)

st.divider()
st.subheader('What is a ' + career)

system_behavior = 'Answer be detailed but as concisely as possible'
prompt = f'''What is a {career} ? '''

# Show the message
st.write(cli.send_request(system_behavior, prompt))
